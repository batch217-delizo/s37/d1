const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js")

router.post("/create",  auth.verify , (req, res) => {
  const userData = auth.decode(req.headers.authorization)
  courseController.addCourse(req.body, userData).then(resultfromController => {
      res.send(resultfromController)
  })
})

module.exports = router