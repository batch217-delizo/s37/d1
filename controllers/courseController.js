const bcrypt = require("bcrypt");
const Course = require("../models/Course.js");
const auth = require("../auth.js");


module.exports.addCourse = async (reqBody,userData) => {
    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    })
    

    if(userData.isAdmin == true){
      const user =  await newCourse.save() 

      if(!user) {
        return false
      } 

        return true
    } else {
      return false
    }

     

}

